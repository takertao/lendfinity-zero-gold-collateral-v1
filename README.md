# LENDFINITY:  Zero Gold Collateral V1 Protocol
Zero Gold Collateral V1 is a composition of smart contracts, which create the protocol. These contracts enable digital asset holders on the Ethereum blockchain to engage in decentralized, lending and borrowing activities, with down to zero collateral.

The protocol is currently in beta and deployed on the Ropsten testnet.

To expand the possibilities of undercollateralized DeFi, we are exploring integrations with https://www.stratosphere.network/. Interoperability with the Stratosphere Blockchain will enable the project to tie directly into the benefits of major cloud resources while maintaining decentralization.

If you would like to contribute, we encourage you to submit a PR directly or join the open-source team at clearsky@skysoltion.org
© Copyright 2020, FCC Labs
